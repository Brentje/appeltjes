﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestingDialogue : MonoBehaviour
{
    private UIStack uistack;
    // Start is called before the first frame update
    void Start()
    {
        uistack = FindObjectOfType<UIStack>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            DialogueBox dialogueBox = (DialogueBox)uistack.Push("DialogueBox").Element;
            dialogueBox.Show(new NPC(), new Dialogue()
            {
                nodes = new List<DialogueNode>()
                {
                    new DialogueNode()
                    {
                        name = "boi", text = "HALLO", options = new List<DialogueOption>()
                        {
                            new DialogueOption("Hello itsa me, big boi option dude", -1),
                            new DialogueOption("Hello itsa me, big boi option dude numba 2", -1),
                            new DialogueOption("Hello itsa me, big boi option dude numba 3", -1),
                        }
                    }
                }
            });

        }
    }
}
