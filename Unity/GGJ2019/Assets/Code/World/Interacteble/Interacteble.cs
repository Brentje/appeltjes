﻿using UnityEngine;
using UnityEngine.Events;

public class Interacteble : MonoBehaviour
{
    public UnityEvent OnCollision;

    public virtual void Interact()
    {
        OnCollision.Invoke();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Interact();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Interact();
    }
}
