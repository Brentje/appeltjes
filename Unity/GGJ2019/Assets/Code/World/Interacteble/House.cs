﻿using System.Collections;
using UnityEngine;
using UnityEngine.Tilemaps;

public class House : MonoBehaviour
{
    public TilemapRenderer roof;
    public GameObject internals;
    private bool current;

    public void Show()
    {
        if (!current)
            StartCoroutine(ChangeState(true, 0.5f));
    }
    public void Hide()
    {
        if (current)
            StartCoroutine(ChangeState(false, 0.5f));
    }
    private IEnumerator ChangeState(bool show, float lerpTime)
    {
        current = show;
        if (show)
            internals.SetActive(show);
        float timer = lerpTime;
        while (timer > 0)
        {
            timer -= Time.deltaTime;
            roof.material.SetColor("_Color", new Color(1, 1, 1, show ? 1 / lerpTime * timer : 1 - (1 / lerpTime * timer)));
            yield return new WaitForSeconds(0);
        }
        if (show)
            internals.SetActive(show);
    }
}
