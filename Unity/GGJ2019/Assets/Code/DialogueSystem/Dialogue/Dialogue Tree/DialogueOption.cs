﻿using System.Collections.Generic;
using UnityEngine.Events;

[System.Serializable]
public class DialogueOption 
{
    //The text displayed on the button of the option
    public string text = "";

    //The node that gets activated when the button is pressed. -1 = exit node
    public int destinationNodeID = -1;

    //A invokable action
    public UnityAction action;


    //Empty constructor for the xml loading
    public DialogueOption()
    {

    }

    //Initialize the dialogue option
    public DialogueOption(string text ,int nodeToActivate)
    {
        this.text = text;
        destinationNodeID = nodeToActivate;
    }
}
