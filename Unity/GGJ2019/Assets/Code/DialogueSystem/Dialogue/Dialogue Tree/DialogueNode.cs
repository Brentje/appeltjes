﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class DialogueNode
{
    //The ID of this node
    public int nodeID = 0;

    //The name of this node
    public string name = "";

    //The text of this node
    public string text;

    //The name of the Quest
    public string QuestName;

    //The options given at the end of the node
    public List<DialogueOption> options;


    //Base constructor for xml Loading
    public DialogueNode()
    {
        options = new List<DialogueOption>();
    }

    //Add an option to the node that allows the player to enter the destination node
    public void AddOption(string text = "", DialogueNode destNode = null)
    {
        //Create an option
        DialogueOption optionToAdd;

        //Set the option value (-1 if exit node)
        if (destNode == null)
            optionToAdd = new DialogueOption(text, -1);
        else
            optionToAdd = new DialogueOption(text, destNode.nodeID);

        //Add the option to the node
        options.Add(optionToAdd);
    }

    //Add an option to the node that allows the player to enter the destination node
    public void AddOption()
    {
        //Create an option
        DialogueOption optionToAdd = new DialogueOption(string.Empty, -1);

        //Add the option to the node
        options.Add(optionToAdd);
    }

    //Add an option to the node that allows the player to enter the destination node
    public void AddOption(string text, int destNodeID)
    {
        //Create an option
        DialogueOption optionToAdd = new DialogueOption(text, destNodeID);

        //Add the option to the node
        options.Add(optionToAdd);
    }

    //Remove an option from the options of this node based on the option given //Note this will remove the first found
    public void RemoveOption(DialogueOption optionToRemove)
    {
        if (options.Contains(optionToRemove))
            options.Remove(optionToRemove);
    }

    //Remove an option from the options of this node based on a index
    public void RemoveOption(uint index)
    {
        if (index < options.Count)
            options.RemoveAt((int)index);
    }
}
