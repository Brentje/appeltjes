﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Name", menuName = "SerializedData/DialogueData")]
public class DialogueData : ScriptableObject
{
    public List<Dialogue> Dialogues;
}
