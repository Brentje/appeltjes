﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;
using System;

public class DialogueCreator : EditorWindow
{
    //The current dialogue being worked on
    public Dialogue currentDialogue;

    // The current ScriptableObject being worked on
    public DialogueData selectedDialogueData;

    //Window width and height
    private float winMinX = 200;
    private float winMinY = 350;

    private List<Vector2> windowScollPosition;
    private List<Vector2> textScollPosition;
    private List<Vector2> optionScollPosition;

    //Drag offset
    private Vector2 offset;

    //Node Lists
    List<Rect> windows = new List<Rect>();

    //The node that is creating a transition
    private int transitionNodeID = -1;

    //Is there a transition being made?
    private bool isMakingTransition = false;

    //All the existing dialogues in the Resources/Dialogues folder
    private Dictionary<string, List<Dialogue>> existingDialogues;

    private string dialogueDataName = "";
    private string currentDialogueName = "";

    //Initialize the editor window
    [MenuItem("Window/Tools/DialogueCreator")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        DialogueCreator window = (DialogueCreator)GetWindow(typeof(DialogueCreator));
        window.titleContent = new GUIContent("Dialogue Editor");
        window.Show();
    }

    private void OnEnable()
    {
        windowScollPosition = new List<Vector2>();
        textScollPosition = new List<Vector2>();
        optionScollPosition = new List<Vector2>();
        existingDialogues = new Dictionary<string, List<Dialogue>>();

        LoadDialogues();
    }

    void OnGUI()
    {
        if (currentDialogue == null)
        {
            GUILayout.BeginHorizontal();

            dialogueDataName = GUILayout.TextField(dialogueDataName, GUILayout.Width(250));

            if (GUILayout.Button("Create New Dialogue", GUILayout.Height(20), GUILayout.Width(150)))
            {
                string path = "Assets/Resources/Dialogues/";
                DialogueData data = CreateInstance<DialogueData>();

                string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + $"/{dialogueDataName}.asset");

                AssetDatabase.CreateAsset(data, assetPathAndName);
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();

                selectedDialogueData = Resources.Load<DialogueData>($"Dialogues/{dialogueDataName}");
                currentDialogue = new Dialogue();
            }
            GUILayout.EndHorizontal();

            DrawLoadedDialogues();
        }
        else
        {
            //Draw the grid
            DrawGrid(10, 0.3f, Color.gray);
            DrawGrid(100, 0.5f, Color.gray);

            //Only run when there are windows
            if (windows.Count > 0)
            {
                //Manage the connections between nodes
                ManageConnections();

                //Draw the windows
                DrawWindows();
            }

            //Check events
            CheckEvents(Event.current);

            //Draw the buttons
            DrawUI();

            //Repaint the editor each frame 
            Repaint();
        }
    }

    private void LoadDialogues()
    {
        DialogueData[] dialogueDatas = Resources.LoadAll<DialogueData>("Dialogues/");

        if (dialogueDatas != null)
        {
            for (int i = 0; i < dialogueDatas.Length; i++)
            {
                string actorName = dialogueDatas[i].name;

                List<Dialogue> dialogues = dialogueDatas[i].Dialogues;

                if (existingDialogues.ContainsKey(actorName))
                    return;

                existingDialogues.Add(actorName, dialogues);
            }
        }
    }

    private void DrawLoadedDialogues()
    {
        foreach (string existingDialoguesKey in existingDialogues.Keys)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label(existingDialoguesKey, GUILayout.Width(150));

            GUILayout.BeginVertical();
            foreach (Dialogue dialogue in existingDialogues[existingDialoguesKey])
            {
                if (GUILayout.Button(dialogue.Name, GUILayout.Height(20), GUILayout.Width(150)))
                {
                    currentDialogue = dialogue;
                    currentDialogueName = dialogue.Name;
                    selectedDialogueData = Resources.Load<DialogueData>($"Dialogues/{existingDialoguesKey}");

                    //Todo Load in the visuals of the loaded dialogue
                    for (int i = 0; i < currentDialogue.nodes.Count; i++)
                    {
                        windows.Add(new Rect(300, 100, winMinX, winMinY));
                        windowScollPosition.Add(new Vector2());
                        textScollPosition.Add(new Vector2());
                        optionScollPosition.Add(new Vector2());
                    }

                    for (int i = 0; i < currentDialogue.nodes.Count; i++)
                    {
                        for (int j = 0; j < currentDialogue.nodes[i].options.Count; j++)
                        {
                            Rect currentNodeWindow = windows[currentDialogue.nodes[i].options[j].destinationNodeID];

                            if (currentDialogue.nodes[i].options.Count % 2 == 0)
                            {
                                int pos = -(currentDialogue.nodes[i].options.Count / 2) + j;

                                if (pos == 0)
                                    currentNodeWindow.position = new Vector2(windows[i].position.x + windows[i].width, windows[i].position.y + windows[i].height);
                                else if (pos > 0)
                                    currentNodeWindow.position = new Vector2(windows[i].position.x + (windows[i].width * pos) + windows[i].width, windows[i].position.y + windows[i].height);
                                else if (pos < 0)
                                    currentNodeWindow.position = new Vector2(windows[i].position.x + (windows[i].width * pos), windows[i].position.y + windows[i].height);
                            }
                            else
                            {
                                int pos = -(currentDialogue.nodes[i].options.Count / 2) + j + 1;

                                if (pos == 0)
                                    currentNodeWindow.position = new Vector2(windows[i].position.x - windows[i].width, windows[i].position.y + windows[i].height);
                                else
                                    currentNodeWindow.position = new Vector2(windows[i].position.x + (windows[i].width * pos) - windows[i].width, windows[i].position.y + windows[i].height);
                            }
                            windows[currentDialogue.nodes[i].options[j].destinationNodeID] = currentNodeWindow;
                        }
                    }
                }
            }

            if (GUILayout.Button("Add", GUILayout.Height(20), GUILayout.Width(150)))
            {
                selectedDialogueData = Resources.Load<DialogueData>($"Dialogues/{existingDialoguesKey}");
                currentDialogue = new Dialogue();
            }

            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
        }
    }

    //Draw the line to the cursor 
    private void DrawCursorConnection(int nodeID)
    {
        if (nodeID < 0 || nodeID >= windows.Count)
            return;

        Vector3 startPos = new Vector3(windows[nodeID].x + windows[nodeID].width / 2, windows[nodeID].y + windows[nodeID].height, 0);
        Vector3 endPos = Event.current.mousePosition;
        Vector3 startTan = startPos + Vector3.up * 100;
        Vector3 endTan = endPos + Vector3.down * 100;
        Color shadowCol = new Color(0, 0, 0, 0.06f);

        //Draw shadows
        for (int i = 0; i < 3; i++)
            Handles.DrawBezier(startPos, endPos, startTan, endTan, shadowCol, null, (i + 1) * 5);

        //Draw the line
        Handles.DrawBezier(startPos, endPos, startTan, endTan, Color.red, null, 1);
    }

    //Draw the buttons in the left upper corner
    private void DrawUI()
    {
        currentDialogueName = GUILayout.TextField(currentDialogueName, GUILayout.Width(250));

        //Button to save the dialogue
        if (GUILayout.Button("Save Changes", GUILayout.Height(20), GUILayout.Width(150)))
        {
            if (selectedDialogueData.Dialogues == null)
                selectedDialogueData.Dialogues = new List<Dialogue>();

            if (currentDialogueName.IsEmptyOrWhiteSpace())
            {
                Debug.Log("Please insert a name for the current dialogue!");
                return;
            }
            Dialogue dialogueToRemove = null;
            foreach (Dialogue dialogue in selectedDialogueData.Dialogues)
                if (dialogue.Name == currentDialogueName)
                    dialogueToRemove = dialogue;

            selectedDialogueData.Dialogues.Remove(dialogueToRemove);
            
            currentDialogue.Name = currentDialogueName;
            selectedDialogueData.Dialogues.Add(currentDialogue);

            EditorUtility.SetDirty(selectedDialogueData);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();

            selectedDialogueData = null;
            currentDialogue = null;

            windows.Clear();

            LoadDialogues();
        }

        if (GUILayout.Button("Back", GUILayout.Height(20), GUILayout.Width(150)))
        {
            //Set the dialogue to null
            currentDialogue = null;
            selectedDialogueData = null;

            windows.Clear();

            LoadDialogues();
        }
    }

    //Manages the lines between nodes
    private void ManageConnections()
    {
        //Draw the connection of the cursor to the node that started the bind
        DrawCursorConnection(transitionNodeID);

        //Draw the bezier curves connecting the nodes based on the options
        for (int i = 0; i < currentDialogue.nodes.Count; i++)
        for (int j = 0; j < currentDialogue.nodes[i].options.Count; j++)
            if (currentDialogue.nodes[i].options[j].destinationNodeID >= 0)
                DrawNodeCurve(windows[i], windows[currentDialogue.nodes[i].options[j].destinationNodeID], i, currentDialogue.nodes[i].options[j].destinationNodeID);
    }

    //Draw the windows
    private void DrawWindows()
    {
        BeginWindows();

        //Draw the windows
        for (int i = 0; i < windows.Count; i++)
            windows[i] = GUILayout.Window(i, windows[i], DrawNodeWindow, currentDialogue.nodes[i].name + " ID: " + currentDialogue.nodes[i].nodeID);

        EndWindows();
    }

    //Get the input
    public void CheckEvents(Event ev)
    {
        switch (ev.type)
        {
            case EventType.MouseDown:

                //Check button 1
                if (ev.button == 1)
                {
                    for (int i = 0; i < windows.Count; i++)
                    {
                        if (windows[i].Contains(ev.mousePosition))
                        {
                            GenericMenuOnNode(ev.mousePosition, i);
                            return;
                        }
                        else
                            GenericMenuOnGrid(Event.current.mousePosition);
                    }

                    if (windows.Count == 0)
                        GenericMenuOnGrid(Event.current.mousePosition);
                }
                break;

            case EventType.KeyDown:
                if (Event.current.keyCode == KeyCode.C && (Event.current.modifiers == EventModifiers.Control || Event.current.modifiers == EventModifiers.Command))
                {
                    Event.current.Use();
                    TextEditor textEditor = (TextEditor)EditorGUIUtility.GetStateObject(typeof(TextEditor), EditorGUIUtility.keyboardControl);
                    textEditor.Copy();
                }
                else if (Event.current.keyCode == KeyCode.V && (Event.current.modifiers == EventModifiers.Control || Event.current.modifiers == EventModifiers.Command))
                {
                    Event.current.Use();
                    TextEditor textEditor = (TextEditor)EditorGUIUtility.GetStateObject(typeof(TextEditor), EditorGUIUtility.keyboardControl);
                    textEditor.Paste();
                }
                break;
            case EventType.MouseDrag:
                Panning(ev.delta);
                Repaint();
                Event.current.Use();
                break;

        }
    }

    //Create a right click menu when clicking on a node
    private void GenericMenuOnNode(Vector2 mousePosition, int nodeID)
    {
        GenericMenu genericMenu = new GenericMenu();
        genericMenu.AddItem(new GUIContent("Make transition"), false, () =>
        {
            isMakingTransition = true;
            transitionNodeID = nodeID;
        });

        genericMenu.AddItem(new GUIContent("Delete Node"), false, () =>
        {
            DeleteNode(nodeID);
        });

        genericMenu.ShowAsContext();
    }

    //Delete a Node
    private void DeleteNode(int id)
    {
        for (int i = 0; i < currentDialogue.nodes.Count; i++)
        for (int j = 0; j < currentDialogue.nodes[i].options.Count; j++)
        {
            if (currentDialogue.nodes[i].options[j].destinationNodeID == id)
            {
                currentDialogue.nodes[i].RemoveOption(currentDialogue.nodes[i].options[j]);
                j--;
                continue;
            }

            if (currentDialogue.nodes[i].options[j].destinationNodeID > id)
                currentDialogue.nodes[i].options[j].destinationNodeID--;
        }

        //Remove the nodes from both the node lists
        windows.Remove(windows[id]);
        currentDialogue.RemoveNode((uint)id);

        windowScollPosition.Remove(windowScollPosition[id]);
        textScollPosition.Remove(textScollPosition[id]);
        optionScollPosition.Remove(optionScollPosition[id]);

        //Loop throught the nodes and reset their id
        for (int i = 0; i < currentDialogue.nodes.Count; i++)
            currentDialogue.nodes[i].nodeID = i;
    }


    //Create a right click menu when clicking on the grid
    private void GenericMenuOnGrid(Vector2 mousePosition)
    {
        GenericMenu genericMenu = new GenericMenu();

        genericMenu.AddItem(new GUIContent("Create Node"), false, () =>
        {
            //create the node
            DialogueNode nodeToAdd = new DialogueNode();

            //Add the node to the node lists
            currentDialogue.AddNode(nodeToAdd);
            windows.Add(new Rect(mousePosition.x, mousePosition.y, winMinX, winMinY));
            windowScollPosition.Add(new Vector2());
            textScollPosition.Add(new Vector2());
            optionScollPosition.Add(new Vector2());
        });

        genericMenu.ShowAsContext();
    }

    //Allow to pan around the grid by dragging around with the mouse
    private void Panning(Vector2 delta)
    {
        //Panning around the grid
        offset += delta;

        //Apply the delta to the position of the windows
        for (int i = 0; i < windows.Count; i++)
        {
            Rect currentWindow = windows[i];
            currentWindow.position += delta;
            windows[i] = currentWindow;
        }
    }

    //Draw a grid using the GUI handles
    private void DrawGrid(float gridSpacing, float gridOpacity, Color gridColor)
    {
        //Calculate how many lines would fit based on the spacing
        int widthDivs = Mathf.CeilToInt(position.width / gridSpacing);
        int heightDivs = Mathf.CeilToInt(position.height / gridSpacing);

        Handles.BeginGUI();
        Handles.color = new Color(gridColor.r, gridColor.g, gridColor.b, gridOpacity);

        //Create a offset based on the offset of the mouse delta
        Vector3 newOffSet = new Vector3(offset.x % gridSpacing, offset.y % gridSpacing, 0);

        //Create the lines of the grid that are vertical
        for (int i = 0; i <= widthDivs; i++)
            Handles.DrawLine(new Vector3(gridSpacing * i, -gridSpacing, 0) + newOffSet, new Vector3(gridSpacing * i, position.height + gridSpacing, 0f) + newOffSet);

        //Create the lines of the grid that are horizontal
        for (int j = 0; j <= heightDivs; j++)
            Handles.DrawLine(new Vector3(-gridSpacing, gridSpacing * j, 0) + newOffSet, new Vector3(position.width + gridSpacing, gridSpacing * j, 0f) + newOffSet);

        Handles.color = Color.white;
        Handles.EndGUI();
    }

    void DrawNodeWindow(int id)
    {
        if (isMakingTransition && Event.current.button == 0 && Event.current.type == EventType.MouseDown && id != transitionNodeID)
        {
            currentDialogue.nodes[transitionNodeID].AddOption("", currentDialogue.nodes[id].nodeID);

            isMakingTransition = false;
            transitionNodeID = -1;
        }

        //Start a scroll view
        windowScollPosition[id] = EditorGUILayout.BeginScrollView(windowScollPosition[id]);

        //Show the Node Id in a horizontal layout
        GUILayout.BeginHorizontal();
        GUILayout.Label("Node ID:", EditorStyles.boldLabel);
        GUILayout.Label(currentDialogue.nodes[id].nodeID.ToString());
        GUILayout.EndHorizontal();

        //Show the node name and set the node name to the input of the textfield
        GUILayout.Label("Node Name:", EditorStyles.boldLabel);
        currentDialogue.nodes[id].name = CustomEditorGUI.TextField(currentDialogue.nodes[id].name);

        //Show the text line of the node
        GUILayout.Label("Node Text:", EditorStyles.boldLabel);
        textScollPosition[id] = EditorGUILayout.BeginScrollView(textScollPosition[id], GUILayout.Height(windows[id].height / 5));

        //Show node line
        GUILayout.BeginHorizontal();
        currentDialogue.nodes[id].text = CustomEditorGUI.TextArea(currentDialogue.nodes[id].text);
        GUILayout.EndHorizontal();

        GUILayout.EndScrollView();

        //Show the node options
        GUILayout.Label("Node Options:", EditorStyles.boldLabel);
        optionScollPosition[id] = EditorGUILayout.BeginScrollView(optionScollPosition[id]);

        for (int i = 0; i < currentDialogue.nodes[id].options.Count; i++)
        {
            currentDialogue.nodes[id].options[i].text = CustomEditorGUI.TextField(currentDialogue.nodes[id].options[i].text);

            GUILayout.BeginHorizontal();

            try
            {
                currentDialogue.nodes[id].options[i].destinationNodeID = int.Parse(CustomEditorGUI.TextField(currentDialogue.nodes[id].options[i].destinationNodeID.ToString()));
            }
            catch { }

            if (GUILayout.Button("X", GUILayout.Width(20)))
                currentDialogue.nodes[id].RemoveOption((uint)i);

            GUILayout.EndHorizontal();

            GUILayout.Space(10);
        }
        EditorGUILayout.EndScrollView();

        //Allow dragging the window if it is in focus
        if (Event.current.button == 0)
            GUI.DragWindow();

        EditorGUILayout.EndScrollView();
    }

    private void ResizeNodeWindow(int id, float deltaX, float deltaY)
    {
        if ((windows[id].width + deltaX) > winMinX)
        {
            Rect tempRect = windows[id];
            tempRect.xMax += deltaX;
            windows[id] = tempRect;
        }

        if ((windows[id].height + deltaY) > winMinY)
        {
            Rect tempRect = windows[id];
            tempRect.yMax += deltaY;
            windows[id] = tempRect;
        }
    }

    //Draw the beziercurves between the rects
    private void DrawNodeCurve(Rect start, Rect end, int parentID, int childID)
    {
        Vector3 startPos = new Vector3(start.x + start.width / 2, start.y + start.height, 0);
        Vector3 endPos = new Vector3(end.x + end.width / 2, end.y, 0);
        Vector3 startTan = startPos + Vector3.up * 100;
        Vector3 endTan = endPos + Vector3.down * 100;
        Color shadowCol = new Color(0, 0, 0, 0.06f);

        //Draw shadows
        for (int i = 0; i < 3; i++)
            Handles.DrawBezier(startPos, endPos, startTan, endTan, shadowCol, null, (i + 1) * 5);

        //Draw the line
        Handles.DrawBezier(startPos, endPos, startTan, endTan, Color.red, null, 1);

        //Draw the button on the line for removing the child
        if (Handles.Button((start.center + end.center) * 0.5f, Quaternion.identity, 4, 8, Handles.RectangleHandleCap))
        {
            for (int i = 0; i < currentDialogue.nodes[parentID].options.Count; i++)
            {
                if (currentDialogue.nodes[parentID].options[i].destinationNodeID == childID)
                    currentDialogue.nodes[parentID].RemoveOption((uint)i);
            }
        }
    }
}