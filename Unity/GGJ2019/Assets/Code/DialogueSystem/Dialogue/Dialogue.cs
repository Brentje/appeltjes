﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Dialogue
{
    //Dialogue name
    public string Name;

    //List of nodes that hold the entire possible conversation
    public List<DialogueNode> nodes;

    //Initialize the dialogue
    public Dialogue()
    {
        nodes = new List<DialogueNode>();
    }

    //Add a node to the nodes list
    public void AddNode(DialogueNode nodeToAdd)
    {
        //If the node to add is null return
        if (nodeToAdd == null)
            return;

        //Add the node to the nodes list
        nodes.Add(nodeToAdd);

        //Set the id equal to the index in the nodes list
        nodeToAdd.nodeID = nodes.IndexOf(nodeToAdd);
    }

    //Remove a node from the list of nodes based on the node given
    public void RemoveNode(DialogueNode nodeToRemove)
    {
        //If the node to remove is null return
        if (nodeToRemove == null)
            return;

        //Remove the node from the nodes list
        nodes.Remove(nodeToRemove);
    }

    //Remove a node from the list of nodes based on a index
    public void RemoveNode(uint index)
    {
        //If the index if under 0 return
        if (index < nodes.Count)
            nodes.RemoveAt((int)index);
    }

    //Get a node from the nodes list based on the name assigned to the node
    public DialogueNode GetNodeByName(string name)
    {
        //Loop through the nodes list and check if the name equals one of the node names
        for (int i = 0; i < nodes.Count; i++)
            if (nodes[i].name == name)
                return nodes[i];

        //Return null if node wasnt found
        return null;
    }
}
