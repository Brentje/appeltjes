﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Utilities;
using Time = Utilities.Time;

enum Scenes
{
    MainMenu,
    InGame
}

public class GameManager : Singleton<GameManager>
{
    [SerializeField]
    private Player player;
    public Player GetPlayer { get { return player; } }
    public UIStack uIStack;
    public Light theSun;

    public Gradient SunColor;

    public static bool IsPaused { get; private set; }
    public bool IsDay { get; private set; }

    public TimeStamp StartingTime;
    public float TimeScale;

    private Time time;

    public void LoadScene(int scene)
    {
        SceneManager.LoadScene(scene);
    }

    protected override void Awake()
    {
        base.Awake();
      
    }

    protected void Start()
    {
        theSun = GameObject.Find("Directional Light").GetComponent<Light>();
        uIStack = FindObjectOfType<UIStack>();
        time = new Time(StartingTime, TimeScale);

        theSun.intensity = Mathf.PingPong(GetCurrentTimeStamp().GetTotalSeconds() / new TimeStamp(12, 0, 0).GetTotalSeconds(), 1);
        theSun.color = SunColor.Evaluate((GetCurrentTimeStamp().GetTotalSeconds() - GetCurrentTimeStamp().Day * 86400) / new TimeStamp(23, 59, 59).GetTotalSeconds());
    }

    private void Update()
    {
        if (!IsPaused)
            WorldTick();

        theSun.color = SunColor.Evaluate((GetCurrentTimeStamp().GetTotalSeconds() - GetCurrentTimeStamp().Day * 86400) / new TimeStamp(23, 59, 59).GetTotalSeconds());

        float x = Mathf.PingPong(GetCurrentTimeStamp().GetTotalSeconds() / new TimeStamp(12, 0, 0).GetTotalSeconds() * 0.5f, 1);
        theSun.intensity = Mathf.Lerp(0, 1, x);
    }

    private void WorldTick()
    {
        time.Tick();

        IsDay = (time.CurrentTime.Hour >= 6 && time.CurrentTime.Hour < 18) ? true : false;
    }

    public void Pause(bool value)
    {
        IsPaused = value;
        if (FindObjectOfType<UIStack>().Contains("PauseMenu"))
            FindObjectOfType<UIStack>().Remove("PauseMenu");
    }
    public TimeStamp GetCurrentTimeStamp() => time.CurrentTime;
}