﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ActorManager : Manager
{
  private static Dictionary<int, Actor> actors = new Dictionary<int, Actor>();

  public override void Awake()
  {
    base.Awake();
  }

  public override void Pause(bool pause)
  {
    base.Pause(pause);
    foreach (Actor actor in actors.Values)
      actor.Pause(pause);
  }

  public static Actor GetActor(int identifier)
  {
    if (!actors.ContainsKey(identifier))
      return null;

    return actors[identifier];
  }

  public static T GetActor<T>(int identifier) where T : class
  {
    if (!actors.ContainsKey(identifier))
      return null;

    return actors[identifier] as T;
  }

  public static void RegisterActor(Actor actor)
  {
    if (actor == null || actors.ContainsKey(actor.gameObject.GetInstanceID()))
      return;

    actors.Add(actor.gameObject.GetInstanceID(), actor);
  }

  public static void RemoveActor(Actor actor)
  {
    if (actor == null || !actors.ContainsKey(actor.gameObject.GetInstanceID()))
      return;

    actors.Remove(actor.gameObject.GetInstanceID());
  }
}