﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.Rendering.PostProcessing;

public class WorldManager : MonoBehaviour
{
    public PostProcessVolume Fader;
    public GameObject[] Tilemaps;

    public void ChangeTilemap(int mapIndex)
    {
        StartCoroutine(FadeToNewTileMap(mapIndex));
    }

    private IEnumerator FadeToNewTileMap(int mapIndex)
    {
        Fader.weight = 0;
        while (Fader.weight < 1)
        {
            Fader.weight += Time.deltaTime;
            yield return new WaitForSeconds(0);
        }
        for (int i = 0; i < Tilemaps.Length; i++)
        {
            Tilemaps[i].SetActive(i == mapIndex);
        }
        if (mapIndex == 0)
        {
            // TODO: changeGrayScale
        }
        while (Fader.weight > 0)
        {
            Fader.weight -= Time.deltaTime;
            yield return new WaitForSeconds(0);
        }
    }
}
