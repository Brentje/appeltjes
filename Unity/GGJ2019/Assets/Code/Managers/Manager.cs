﻿using UnityEngine;
using System.Collections;

public class Manager
{
  public virtual void Awake()
  {

  }

  public virtual void Start()
  {

  }

  public virtual void Update()
  {

  }

  virtual public void OnLoadScene()
  {

  }

  virtual public void Pause(bool pause)
  {

  }
}
