﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using Utilities;

public class Activity
{
    public string Name;
    public TimeStamp TimeStamp;
    public TimeStamp Duration;

    private TimeStamp earlyMargin;
    private TimeStamp lateMargin;

    private Conditions condition;

    private UnityAction onComplete;
    private UnityAction onFailed;

    public Activity(string Name, TimeStamp TimeStamp, Conditions Condition = Conditions.ON_TIME)
    {
        this.Name = Name;
        this.TimeStamp = TimeStamp;
        this.condition = Condition;

        earlyMargin = new TimeStamp(0, 5, 0);
        lateMargin = new TimeStamp(0, 5, 0);

        SetArrivalMargin();
    }

    public virtual void Update()
    {
        TimeStamp currentTime = GameManager.Instance.GetCurrentTimeStamp();

        if (currentTime >= TimeStamp - earlyMargin && currentTime <= TimeStamp + lateMargin)
            Complete();
        else if (currentTime > TimeStamp + lateMargin)
            Fail();
    }

    private void SetArrivalMargin()
    {
        if (condition.HasFlag(Conditions.ARRIVE_EARLY))
            earlyMargin.Minute = 20;
        if (condition.HasFlag(Conditions.ARRIVE_LATE))
            lateMargin.Minute = 20;
    }

    public string GetName() => Name;
    public TimeStamp GetTimeStamp() => TimeStamp;
    public void SetOnCompleteCallback(UnityAction Action) => onComplete += Action;
    public void SetOnFailedCallback(UnityAction Action) => onFailed += Action;
    public void Complete() => onComplete?.Invoke();
    public void Fail() => onFailed?.Invoke();
}
