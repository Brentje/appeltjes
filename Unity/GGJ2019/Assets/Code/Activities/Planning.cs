﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;
using Time = Utilities.Time;

public class Planning
{
  public List<List<Activity>> Activities;
  private Calender calender;

  public Planning()
  {
    Activities = new List<List<Activity>>
        {
            new List<Activity>(),
            new List<Activity>(),
            new List<Activity>(),
            new List<Activity>(),
            new List<Activity>(),
            new List<Activity>(),
            new List<Activity>()
        };
  }

  public void Update()
  {
    for (int i = 0; i < Activities.Count; i++)
    {
      for (int j = 0; j < Activities[i].Count; j++)
      {
        Activity activity = Activities[i][j];
        activity.Update();

        if (Activities[i].Contains(activity))
          i--;
      }
    }
  }

  public void SetCalender(Calender calender)
  {
    this.calender = calender;
  }

  public Activity AddActivity<T>(Time.DaysOfTheWeek Day, T Activity, bool addToCalender = false) where T : Activity
  {
    if (addToCalender)
      calender.AddActivity(Activity);
    Activities[(int)Day].Add(Activity);
    return Activity;
  }

  public void RemoveActivity(Activity Activity)
  {
    List<Activity> markedActivities = new List<Activity>();

    foreach (List<Activity> activities in Activities)
      foreach (Activity activity in activities)
      {
        if (activity == Activity)
          markedActivities.Add(activity);
      }

    foreach (Activity markedActivity in markedActivities)
      Activities[markedActivity.TimeStamp.Day].Remove(markedActivity);
  }

  public List<Activity> GetActivitiesPerDay(Time.DaysOfTheWeek Day)
  {
    return Activities[(int)Day];
  }

  public T GetActivity<T>(Time.DaysOfTheWeek Day, T Activity) where T : Activity
  {
    return (T)Activities[(int)Day][0];
  }
}
