﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Location
{
    public string Name;
    public Vector3 Position;

    public Location(string Name, Vector3 Position)
    {
        this.Name = Name;
        this.Position = Position;
    }
}
