﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;
using Time = Utilities.Time;

public class LocationActivity : Activity
{
    public Location Location { get; private set; }

    public LocationActivity(string Name, TimeStamp TimeStamp, Location Location, Conditions Condition = Conditions.ON_TIME) : base(Name, TimeStamp, Condition)
    {
        this.Location = Location;
    }

    public override void Update()
    {
        if (Vector3.Distance(GameManager.Instance.GetPlayer.transform.position, Location.Position) <= 2)
            base.Update();
    }
}

[Flags]
public enum Conditions
{
    ON_TIME,
    ARRIVE_EARLY,
    ARRIVE_LATE
}