﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

public class NPCActivity : Activity
{
    public NPC NPC { get; private set; }

    public NPCActivity(string Name, TimeStamp TimeStamp, NPC NPC, Conditions Condition) : base(Name, TimeStamp, Condition)
    {
        this.NPC = NPC;
    }

    public override void Update()
    {
        throw new System.NotImplementedException();
    }
}
