﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : UIElement
{
    public void Resume()
    {
        GameManager.Instance.Pause(false);
    }

    public void BackToMain()
    {
        GameManager.Instance.LoadScene(0);
    }

    public void QuitGame()
    {
        Application.Quit(0);
    }
}
