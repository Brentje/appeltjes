﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueBox : UIElement
{
    public Text DialogueText;
    public GameObject OptionsPanel;
    public Button DialogueOptionButton;

    private string playerName;
    private string npcName;

    private List<Button> Options;

    private Dialogue currentDialogue;

    public override void Enter()
    {
        base.Enter();

        Options = new List<Button>();

        playerName = "Robin";

    }

    public void Show(NPC npc, Dialogue dialogue)
    {
        currentDialogue = dialogue;
        npcName = npc.Name;

        DrawContent(dialogue.nodes[0]);
    }

    private void DrawContent(DialogueNode dialogueNode)
    {
        Clear();

        DialogueText.text = "[" + dialogueNode.name + "] " + dialogueNode.text;

        OptionsPanel.SetActive(dialogueNode.options.Count != 0);

        for (int i = 0; i < dialogueNode.options.Count; i++)
        {
            Debug.Log(dialogueNode.options.Count);
            Button option = Instantiate(DialogueOptionButton, OptionsPanel.GetComponent<RectTransform>());
            option.gameObject.SetActive(true);
            option.GetComponentInChildren<Text>().text = (i + 1) + ". " + dialogueNode.options[i].text;

            int index = i;
            option.onClick.AddListener(() =>
            {
                DrawContent(currentDialogue.nodes[dialogueNode.options[index].destinationNodeID]);

                if (dialogueNode.options[index].action != null)
                    dialogueNode.options[index].action.Invoke();
            });
            Options.Add(option);
        }
    }

    private void Clear()
    {
        if (Options == null)
            return;

        foreach (Button option in Options)
            Destroy(option.gameObject);

        Options.Clear();
    }

    public override void Exit()
    {
        base.Exit();

    }
}