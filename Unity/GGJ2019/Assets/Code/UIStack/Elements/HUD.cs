﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD : UIElement
{
    [SerializeField]
    private Sprite[] smileys;

    [SerializeField]
    private Image image;

    public void UpdateMoodSmiley(int currentMood, int maxMood)
    {
        float moodPerc = (float)currentMood / (float)maxMood;

        int index = 0 ;// = (int)(((float)1 / moodPerc) / (float)smileys.Length);

        if (moodPerc < 0.2f)
        {
            index = 4;
        }
        else if (moodPerc < 0.4f)
        {
            index = 3;
        }
        else if (moodPerc < 0.6f)
        {
            index = 2;
        }
        else if (moodPerc < 0.8f)
        {
            index = 1;
        }
        else if (moodPerc < 1)
        {
            index = 0;
        }

        image.sprite = smileys[index];
    }
}
