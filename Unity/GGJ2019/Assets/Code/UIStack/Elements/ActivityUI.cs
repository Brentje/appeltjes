﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActivityUI : MonoBehaviour
{
  [SerializeField]
  private Text title;
  [SerializeField]
  private Text time;
  [SerializeField]
  private Text location;

  public void SetActivityUI(string title, string time, string location = "")
  {
    this.title.text = title;
    this.time.text = time;
    this.location.text = location;
  }
}
