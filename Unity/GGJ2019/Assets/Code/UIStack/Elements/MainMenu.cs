﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : UIElement
{

    public override void Enter()
    {
        base.Enter();
    }

    public override void Exit()
    {
        base.Exit();
    }

    public void Play()
    {
        GameManager.Instance.LoadScene(1);
    }

    public void Quit()
    {
        Application.Quit(0);
    }
}
