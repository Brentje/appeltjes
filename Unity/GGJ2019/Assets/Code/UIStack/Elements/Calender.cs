﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Calender : UIElement
{
    private int[] monthDayCounts;

    [SerializeField]
    private Button nextMonthButton;
    [SerializeField]
    private Button prevMonthButton;

    [SerializeField]
    private GameObject dayPrefab;

    [SerializeField]
    private GameObject dayPanel;

    [SerializeField]
    private Text currentMonthText;

    [SerializeField]
    private ActivityUI activityUI;

    private Month currentMonth = Month.JANUARY;

    public Calender()
    {
        monthDayCounts = new int[]
        {
            31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
        };
    }

    private List<Activity> activities = new List<Activity>();

    public void AddActivity(Activity activity)
    {
        activities.Add(activity);
    }

    private void LoadActivities()
    {
        print("LoadActivities " + activities.Count);
        foreach (Activity activity in activities)
        {
            ActivityUI activityUIInstance = Instantiate(activityUI, dayPanel.transform.GetChild(0).GetComponent<RectTransform>());
            print("LoadActivities " + activity);
            if (typeof(LocationActivity).IsInstanceOfType(activity))
            {
                LocationActivity locationActivity = (LocationActivity)activity;
                activityUIInstance.SetActivityUI(locationActivity.Name, locationActivity.TimeStamp.ToString(), locationActivity.Location.ToString());
            }
            else
                activityUIInstance.SetActivityUI(activity.Name, activity.TimeStamp.ToString());
        }
    }

    public override void Enter()
    {
        base.Enter();

        nextMonthButton.onClick.AddListener(() => SetCurrentMonth(currentMonth + 1));
        prevMonthButton.onClick.AddListener(() => SetCurrentMonth(currentMonth - 1));

        UpdateCalender();
    }

    public void SetCurrentMonth(Month newMonth)
    {
        if (newMonth < Month.JANUARY || newMonth > Month.DECEMBER)
            return;

        for (int i = 0; i < dayPanel.transform.childCount; i++)
        {
            Destroy(dayPanel.transform.GetChild(i).gameObject);
        }
        currentMonth = newMonth;
        UpdateCalender();
    }

    private void UpdateCalender()
    {
        for (int i = 0; i < monthDayCounts[(int)currentMonth]; i++)
        {
            GameObject currentDayPanel = Instantiate(dayPrefab, dayPanel.GetComponent<RectTransform>());
            currentDayPanel.GetComponentInChildren<Text>().text = (i + 1).ToString();
        }
        LoadActivities();
        currentMonthText.text = currentMonth.ToString();
    }
}

public enum Month
{
    JANUARY,
    FEBRUARY,
    MARCH,
    APRIL,
    MAY,
    JUNE,
    JULY,
    AUGUST,
    SEPTEMBER,
    OKTOBER,
    NOVEMBER,
    DECEMBER
}