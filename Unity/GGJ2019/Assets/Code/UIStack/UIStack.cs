﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIStack : MonoBehaviour
{
    public UIObject[] UIObjects;
    private List<UIObject> uiStack;

    public UIStack()
    {
        uiStack = new List<UIObject>();
    }

    private void Awake()
    {
        for (int i = 0; i < UIObjects.Length; i++)
            if (UIObjects[i].ActiveOnSceneLoad)
                Push(UIObjects[i].Name);
    }

    //Push a UIElement onto the UI Stack
    public UIObject Push(string uiObjectName)
    {
        if (string.IsNullOrEmpty(uiObjectName))
            return null;

        for (int i = 0; i < uiStack.Count; i++)
            if (uiStack[i].Name == uiObjectName)
                RemoveFromList(uiStack[i]);

        UIObject uiObject = GetCopyOfUIObjectByName(uiObjectName);

        if (uiObject != null)
        {
            uiObject.Element = Instantiate(uiObject.Element, transform);

            uiObject.Element.uiStack = this;

            if (Peek() != null)
                Peek().Element.Exit();

            uiObject.Element.Enter();

            uiStack.Add(uiObject);

            return uiObject;
        }

        return null;
    }

    //Pop a UIElement from the UI Stack
    public void Pop()
    {
        if (uiStack.Count == 0)
            return;

        RemoveFromList(Peek());

        if (Peek() != null)
            Peek().Element.Enter();
    }

    public UIObject Peek()
    {
        if (uiStack.Count > 0)
            return uiStack[uiStack.Count - 1];

        return null;
    }

    public void PopAll(params string[] exceptions)
    {
        if (uiStack.Count == 0)
            return;

        for (int i = uiStack.Count - 1; i >= 0; i--)
        {
            if (exceptions.Contains(uiStack[i].Name))
                continue;

            Remove(uiStack[i].Element);
        }
    }

    public bool Contains(string uiObjectName)
    {
        for (int i = 0; i < uiStack.Count; i++)
            if (uiStack[i].Name == uiObjectName)
                return true;
        return false;
    }

    public void Remove(UIElement uiElementToRemove)
    {
        if (uiElementToRemove == null)
            return;

        for (int i = 0; i < uiStack.Count; i++)
            if (uiStack[i].Element == uiElementToRemove)
                RemoveFromList(uiStack[i]);
    }

    public void Remove(string uiElementToRemove)
    {
        if (string.IsNullOrEmpty(uiElementToRemove))
            return;

        for (int i = 0; i < uiStack.Count; i++)
            if (uiStack[i].Name == uiElementToRemove)
                RemoveFromList(uiStack[i]);
    }

    private void RemoveFromList(UIObject uiObjectToRemove)
    {
        if (uiObjectToRemove == null || !uiStack.Contains(uiObjectToRemove))
            return;

        uiObjectToRemove.Element.Exit();

        uiStack.Remove(uiObjectToRemove);

        if (uiObjectToRemove.Element)
            Destroy(uiObjectToRemove.Element.gameObject);
    }

    private UIObject GetCopyOfUIObjectByName(string uiObjectname)
    {
        for (int i = 0; i < UIObjects.Length; i++)
            if (UIObjects[i].Name == uiObjectname)
                return new UIObject
                {
                    Name = UIObjects[i].Name,
                    Element = UIObjects[i].Element
                };

        return null;
    }
}

[System.Serializable]
public class UIObject
{
    public string Name;
    public UIElement Element;
    public bool ActiveOnSceneLoad = false;
}