﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class CustomEditorGUI
{
    public static string TextArea(string value, params GUILayoutOption[] options)
    {
        int textAreaID = GUIUtility.GetControlID("TextArea".GetHashCode(), FocusType.Keyboard) + 1;
        if (textAreaID == 0)
            return value;

        // Handle custom copy-paste
        value = HandleCopyPaste(textAreaID) ?? value;

        return GUILayout.TextArea(value);
    }

    public static string TextField(string value, params GUILayoutOption[] options)
    {
        int textFieldID = GUIUtility.GetControlID("TextField".GetHashCode(), FocusType.Keyboard) + 1;
        if (textFieldID == 0)
            return value;

        // Handle custom copy-paste
        value = HandleCopyPaste(textFieldID) ?? value;

        return GUILayout.TextField(value, options);
    }

    public static int IntField(GUIContent label, int value, params GUILayoutOption[] options)
    {
        int textAreaID = GUIUtility.GetControlID("IntField".GetHashCode(), FocusType.Keyboard) + 1;
        if (textAreaID == 0)
            return value;

        int result;
        // Handle custom copy-paste
        if (int.TryParse(HandleCopyPaste(textAreaID), out result)) 
        value = result;

        return EditorGUILayout.IntField(label, value, options);
    }

    public static int IntField(int value, params GUILayoutOption[] options)
    {
        int textAreaID = GUIUtility.GetControlID("IntField".GetHashCode(), FocusType.Keyboard) + 1;
        if (textAreaID == 0)
            return value;

        int result;
        // Handle custom copy-paste
        if (int.TryParse(HandleCopyPaste(textAreaID), out result)) 
        value = result;

        return EditorGUILayout.IntField(value, options);
    }

    public static float FloatField(GUIContent label, float value, params GUILayoutOption[] options)
    {
        int textAreaID = GUIUtility.GetControlID("FloatField".GetHashCode(), FocusType.Keyboard) + 1;
        if (textAreaID == 0)
            return value;

        float result;
        // Handle custom copy-paste
        if (float.TryParse(HandleCopyPaste(textAreaID), out result))
            value = result;

        return EditorGUILayout.FloatField(label, value, options);
    }

    public static float FloatField(float value, params GUILayoutOption[] options)
    {
        int textAreaID = GUIUtility.GetControlID("FloatField".GetHashCode(), FocusType.Keyboard) + 1;
        if (textAreaID == 0)
            return value;

        float result;
        // Handle custom copy-paste
        if (float.TryParse(HandleCopyPaste(textAreaID), out result))
            value = result;

        return EditorGUILayout.FloatField(value, options);
    }

    private static string HandleCopyPaste(int controlID)
    {
        if (controlID == GUIUtility.keyboardControl)
        {
            if (Event.current.type == EventType.KeyUp && (Event.current.modifiers == EventModifiers.Control || Event.current.modifiers == EventModifiers.Command))
            {
                if (Event.current.keyCode == KeyCode.C)
                {
                    Event.current.Use();
                    TextEditor editor = (TextEditor)GUIUtility.GetStateObject(typeof(TextEditor), GUIUtility.keyboardControl);
                    editor.Copy();
                }
                else if (Event.current.keyCode == KeyCode.V)
                {
                    Event.current.Use();
                    TextEditor editor = (TextEditor)GUIUtility.GetStateObject(typeof(TextEditor), GUIUtility.keyboardControl);
                    editor.Paste();
                    return editor.text;
                }
                else if (Event.current.keyCode == KeyCode.X)
                {
                    Event.current.Use();
                    TextEditor editor = (TextEditor)GUIUtility.GetStateObject(typeof(TextEditor), GUIUtility.keyboardControl);
                    editor.Cut();
                }
                else if (Event.current.keyCode == KeyCode.A)
                {
                    Event.current.Use();
                    TextEditor editor = (TextEditor)GUIUtility.GetStateObject(typeof(TextEditor), GUIUtility.keyboardControl);
                    editor.SelectAll();
                }
            }
        }
        return null;
    }
}
