﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using UnityEngine;

namespace Utilities {
    public class Singleton<T> : MonoBehaviour where T : Singleton<T> {
        private static T _instance;
        private static readonly object _lock = new object();

        public static T Instance {
            get {
                if (_instance == null) {
                    lock (_lock) {
                        _instance = (T)FindObjectOfType(typeof(T));

                        if (_instance == null) {
                            GameObject singleton = new GameObject();
                            _instance = singleton.AddComponent<T>();
                            singleton.name = "(singleton) " + typeof(T);

                            DontDestroyOnLoad(singleton);
                        }
                    }
                }

                return _instance;
            }
        }

        protected virtual void Awake() {
            if (_instance != null) {
                Destroy(gameObject);
                return;
            }

            _instance = (T)this;
            gameObject.name = "(singleton) " + typeof(T);
            DontDestroyOnLoad(gameObject);
        }
    }

    public class SingletonThreaded<T> : MonoBehaviour where T : SingletonThreaded<T> {
        private static volatile SingletonThreaded<T> _instance;
        private static readonly object _syncRoot = new object();

        public static SingletonThreaded<T> Instance {
            get {
                if (_instance == null) {
                    lock (_syncRoot) {
                        if (_instance == null) {
                            _instance = new SingletonThreaded<T>();
                        }
                    }
                }

                return _instance;
            }
        }
    }

    public class SingletonWithAction<T> : MonoBehaviour where T : SingletonWithAction<T> {
        private static T _instance;
        private static object _lock = new object();
        private static bool _applicationIsQuitting;

        public static T Instance {
            get {
                if (_applicationIsQuitting)
                    return null;

                lock (_lock) {
                    if (_instance == null) {
                        _instance = (T)FindObjectOfType(typeof(T));

                        if (_instance == null) {
                            GameObject singleton = new GameObject();
                            _instance = singleton.AddComponent<T>();
                            singleton.name = "(singleton) " + typeof(T);

                            DontDestroyOnLoad(singleton);
                        }
                    }
                    _instance.OnGet();
                    return _instance;
                }
            }
        }

        protected virtual void OnGet() {

        }

        public void OnDestroy() {
            //_applicationIsQuitting = true;
        }
    }
}
