﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions.Must;

namespace Utilities
{
    public class Time
    {
        public TimeStamp CurrentTime { get; private set; }
        public float TimeScale { get; private set; }

        public float TotalSecondsPassed { get; private set;}

        private float deltaTime;

        public Time()
        {
            this.TimeScale = 1;
            this.CurrentTime = new TimeStamp(0, 0, 0, 0);
        }

        public Time(TimeStamp TimeStamp, float TimeScale)
        {
            this.CurrentTime = TimeStamp;
            this.TimeScale = TimeScale;
            this.TotalSecondsPassed = TimeStamp.GetTotalSeconds();
        }

        public void Tick()
        {
            TotalSecondsPassed += UnityEngine.Time.deltaTime * TimeScale;

            CurrentTime.Day = (int)Mathf.Floor(CurrentTime.Hour / 24);
            CurrentTime.Hour = (int)Mathf.Floor(CurrentTime.Minute / 60);
            CurrentTime.Minute = (int)Mathf.Floor(TotalSecondsPassed / 60);
            CurrentTime.Second = (int)Mathf.Floor(TotalSecondsPassed % 60);
        }

        public void SetTimeScale(float value)
        {
            TimeScale = value;
        }

        public enum DaysOfTheWeek
        {
            Monday = 0,
            Tuesday = 1,
            Wednesday = 2,
            Thursday = 3,
            Friday = 4,
            Saturday = 5,
            Sunday = 6
        }
    }

    [Serializable]
    public class TimeStamp
    {
        public int Day;
        public int Hour;
        public int Minute;
        public float Second;

        public TimeStamp(Time.DaysOfTheWeek Day, int Hour, int Minute, float Seconds)
        {
            this.Day = (int) Day;
            this.Hour = Hour;
            this.Minute = Minute;
            this.Second = Seconds;
        }

        public TimeStamp(int Hour, int Minute, float Seconds)
        {
            this.Day = GameManager.Instance.GetCurrentTimeStamp().Day;
            this.Hour = Hour;
            this.Minute = Minute;
            this.Second = Seconds;
        }

        public float GetTotalSeconds()
        {
            return 86400 * Day + 3600 * Hour + 60 * Minute + Second;
        }

        public override string ToString()
        {
            return
                $"{Enum.GetName(typeof(Time.DaysOfTheWeek), (Time.DaysOfTheWeek) Day)} - {Hour:00}:{Minute:00}:{Second:00}";
        }

        public static bool operator > (TimeStamp a, TimeStamp b)
        {
            return a.GetTotalSeconds() > b.GetTotalSeconds();
        }

        public static bool operator < (TimeStamp a, TimeStamp b)
        {
            return a.GetTotalSeconds() < b.GetTotalSeconds();
        }

        public static bool operator >= (TimeStamp a, TimeStamp b)
        {
            return a.GetTotalSeconds() >= b.GetTotalSeconds();
        }

        public static bool operator <= (TimeStamp a, TimeStamp b)
        {
            return a.GetTotalSeconds() <= b.GetTotalSeconds();
        }

        public static TimeStamp operator + (TimeStamp a, TimeStamp b)
        {
            return new TimeStamp((Time.DaysOfTheWeek) a.Day + b.Day,
                a.Hour + b.Hour, a.Minute + b.Minute, a.Second + b.Second);
        }

        public static TimeStamp operator - (TimeStamp a, TimeStamp b)
        {
            return new TimeStamp((Time.DaysOfTheWeek) a.Day - b.Day,
                a.Hour - b.Hour, a.Minute - b.Minute, a.Second - b.Second);
        }

        public static TimeStamp operator / (TimeStamp a, TimeStamp b)
        {
            return TotalSecondsToTimeStamp(a.GetTotalSeconds() / b.GetTotalSeconds());
        }

        public static TimeStamp TotalSecondsToTimeStamp(float TotalSeconds)
        {
            int second =(int)Mathf.Floor(TotalSeconds % 60);
            int minute = (int)Mathf.Floor(second / 60);
            int hour = (int)Mathf.Floor(minute / 60);
            int day = (int)Mathf.Floor(hour / 24);

            return new TimeStamp((Time.DaysOfTheWeek)day, hour, minute, second);
        }
    }
}