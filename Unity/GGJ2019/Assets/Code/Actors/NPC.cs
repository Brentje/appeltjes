﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC : Character
{
  public string Name { get; set; }

  private List<Dialogue> dialogues;

  public int CurrentDialogueIndex = 0;

  public NPC()
  {
    dialogues = new List<Dialogue>();
  }

  protected void Awake()
  {
        dialogues = //Resources.Load<DialogueData>($"Dialogues/NPC").Dialogues; 
                new List<Dialogue>()
                {
                    new Dialogue()
                    {
                        Name = "NPC",
                        nodes = new List<DialogueNode>()
                        {
                            new DialogueNode()
                            {
                                nodeID = 0,
                                name = "Mom",
                                text = "Hello, my child! Make sure you get to the doctor in time today at 16:00!",
                                options = new List<DialogueOption>()
                                {
                                    new DialogueOption()
                                    {
                                        text = "I will, Mom. Thanks.",
                                        destinationNodeID = 1,
                                        action = () =>{ }
                                    }
                                }
                            },

                            new DialogueNode()
                            {
                                nodeID = 1,
                                name = "Desiree",
                                text = "I will be on time, Mom. Thanks for the reminder!",
                            }
                        }
                    }
                };
  }

  protected override void Start()
  {
    base.Start();
    SwitchWalkDirection();
  }

  private void OnCollisionEnter2D(Collision2D collision)
  {
    SwitchWalkDirection();
  }

  //private void OnTriggerEnter2D(Collider2D collision)
  //  {
  //      SwitchWalkDirection();
  //  }

  public void Update()
  {
    //if (Input.GetKeyDown(KeyCode.Space))
    //{
    //    OpenDialogue();
    //}
  }


  private void SwitchWalkDirection()
  {
    if (moveDirection == 1)
      moveDirection = -1;
    else
      moveDirection = 1;
    rigidBody2D.velocity = transform.right * moveDirection * movementSpeed * Time.deltaTime;
    currentVelocity = rigidBody2D.velocity;
  }

  public void ToggleDialogue(bool Toggle)
  {
    //rigidBody2D.velocity = !Toggle;

    if(Toggle)
    {
      rigidBody2D.velocity = Vector2.zero;
      DialogueBox dialogueBox = (DialogueBox)GameManager.Instance.uIStack.Push("DialogueBox").Element;
      dialogueBox.Show(new NPC(), dialogues[CurrentDialogueIndex]);
    }
    else
    {
      rigidBody2D.velocity = currentVelocity;
      GameManager.Instance.uIStack.Remove("DialogueBox"); // ("DialogueBox");
    }
  }
}
