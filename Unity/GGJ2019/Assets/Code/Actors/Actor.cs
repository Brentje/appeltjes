﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Actor : MonoBehaviour
{
    protected virtual void Start()
    {
        ActorManager.RegisterActor(this);
    }

    public virtual void Pause(bool pause)
    {

    }

    protected virtual void OnDestroy()
    {
        ActorManager.RemoveActor(this);
    }
}
