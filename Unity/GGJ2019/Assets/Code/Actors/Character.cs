﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : Actor
{
  [SerializeField]
  protected Rigidbody2D rigidBody2D;
  protected float moveDirection = -1;

  [SerializeField]
  protected float movementSpeed = 300;

  protected Vector3 currentVelocity;

  public int mood { get; protected set; }
  protected int maxMood = 100;

  public override void Pause(bool pause)
  {
    base.Pause(pause);
    rigidBody2D.isKinematic = pause;
  }

  public virtual void UpdateMood(int happiness)
  {
    mood += happiness;
    if (mood >= maxMood)
      mood = maxMood;
  }
}
