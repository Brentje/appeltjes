﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

enum InputState
{
    Default,
    InMenu,
    interacting
}


public class Player : Character
{
    private Planning planning = new Planning();
    [SerializeField]
    private Material grayScaleMaterial;
    private Timer timer = new Timer();
    private IEnumerator moodIEnumerator;
    private bool interactionTekstIsActive;
    private bool isInteracting;
    private NPC npc;
    private InputState inputState;
    private Vector2 movementInput;
    private HUD hud;
    private Calender calender;

    protected override void Start()
    {
        base.Start();
        hud = (HUD)FindObjectOfType<UIStack>().Push("HUD").Element;
        Utilities.TimeStamp currentTime = GameManager.Instance.GetCurrentTimeStamp();
        LocationActivity activity = new LocationActivity("Laundry pickup", new Utilities.TimeStamp((Utilities.Time.DaysOfTheWeek)currentTime.Day, currentTime.Hour, currentTime.Minute, currentTime.Second + 10),
          new Location("Laundromat", transform.position));
        activity.SetOnCompleteCallback(() => UpdateMood(10));
        activity.SetOnFailedCallback(() => UpdateMood(-10));
        //planning.AddActivity((Utilities.Time.DaysOfTheWeek)currentTime.Day, activity, true);
        grayScaleMaterial.SetFloat("_EffectAmount", mood / maxMood);
    }

    public override void Pause(bool pause)
    {
        base.Pause(pause);
    }

    private void Update()
    {
        rigidBody2D.velocity = new Vector2(movementInput.x * movementSpeed * Time.deltaTime, movementInput.y * movementSpeed * Time.deltaTime);
        if (transform.localScale.x != rigidBody2D.velocity.normalized.x)
        {

            if (rigidBody2D.velocity.normalized.x > 0)
                moveDirection = 1;
            else if (rigidBody2D.velocity.normalized.x < 0)
                moveDirection = -1;
        }

        transform.localScale = new Vector3(moveDirection, transform.localScale.y, transform.localScale.z);
        //print(GameManager.Instance.GetCurrentTimeStamp());

        //print(planning.GetActivitiesPerDay((Utilities.Time.DaysOfTheWeek)GameManager.Instance.GetCurrentTimeStamp().Day).Count);

        Debug.DrawRay(transform.position + transform.right * 0.55f * moveDirection,
          transform.right * moveDirection, Color.blue);

        RaycastHit2D hit = Physics2D.Raycast(transform.position + transform.right * 0.55f * moveDirection,
          transform.right, moveDirection * 2);


        if (hit.collider != null && hit.collider.transform.tag == "Character")
        {
            npc = ActorManager.GetActor<NPC>(hit.collider.gameObject.GetInstanceID());
            interactionTekstIsActive = true;
            GameManager.Instance.uIStack.Push("InteractionText");
        }
        else if (!isInteracting)
        {
            interactionTekstIsActive = false;
            FindObjectOfType<UIStack>().Remove("InteractionText");
        }

        CheckInput();
    }

    private void CheckInput()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (FindObjectOfType<UIStack>().Contains("PauseMenu"))
                FindObjectOfType<UIStack>().Remove("PauseMenu");
            else
                FindObjectOfType<UIStack>().Push("PauseMenu");
        }
        switch (inputState)
        {
            case InputState.Default:
                if (interactionTekstIsActive && Input.GetKeyDown(KeyCode.E))
                    Interact(true);

                movementInput = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
                if (Input.GetKeyDown(KeyCode.Tab))
                {
                    if (calender == null)
                    {
                        calender = (Calender)FindObjectOfType<UIStack>().Push("Calendar").Element;
                        planning.SetCalender(calender);
                    }
                    else
                        GameManager.Instance.uIStack.Push("Calendar");

                    inputState = InputState.InMenu;
                    StopMovement();
                }
                break;
            case InputState.InMenu:
                if (Input.GetKeyDown(KeyCode.Tab))
                {
                    GameManager.Instance.uIStack.Remove("Calendar");
                    inputState = InputState.Default;
                }
                if (Input.GetMouseButtonDown(1) && planning.GetActivitiesPerDay((Utilities.Time.DaysOfTheWeek)GameManager.Instance.GetCurrentTimeStamp().Day).Count != 0)
                    planning.GetActivitiesPerDay((Utilities.Time.DaysOfTheWeek)GameManager.Instance.GetCurrentTimeStamp().Day)[0].Complete();

                if (Input.GetMouseButtonDown(0))
                {
                    Utilities.TimeStamp currentTime = GameManager.Instance.GetCurrentTimeStamp();
                    Activity activity = new Activity("Laundry pickup", currentTime + new Utilities.TimeStamp(0, 0, 10));
                    activity.SetOnCompleteCallback(() => UpdateMood(10));
                    activity.SetOnFailedCallback(() => UpdateMood(-10));
                    planning.AddActivity((Utilities.Time.DaysOfTheWeek)currentTime.Day, activity, true);
                }
                break;
            case InputState.interacting:
                if (Input.GetKeyDown(KeyCode.E))
                    Interact(false);
                StopMovement();
                break;
        }
    }

    private void StopMovement()
    {
        rigidBody2D.velocity = Vector2.zero;
        movementInput = Vector2.zero;
    }

    private void Interact(bool toggle)
    {
        rigidBody2D.simulated = !toggle;
        npc.ToggleDialogue(toggle);
        isInteracting = toggle;
        if (toggle)
            inputState = InputState.interacting;
        else
            inputState = InputState.Default;
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
    }

    public override void UpdateMood(int happiness)
    {
        base.UpdateMood(happiness);
        if (moodIEnumerator != null)
            StopCoroutine(moodIEnumerator);
        if (mood >= maxMood)
            return;
        //print("UpdateMood " + happiness);

        StartCoroutine(MoodFade(happiness));
    }

    private IEnumerator MoodFade(int happiness)
    {
        float currentMoodPercentage = mood / (float)maxMood;
        float extraMoodPercentage = happiness / (float)maxMood;
        timer.SetTimer(1);
        while (!timer.TimerDone())
        {
            //print(extraMoodPercentage * timer.TimerProgress());
            grayScaleMaterial.SetFloat("_EffectAmount", currentMoodPercentage + (extraMoodPercentage * timer.TimerProgress()));

            hud.UpdateMoodSmiley(mood, maxMood);
            yield return null;
        }
    }
}
