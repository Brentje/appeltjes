﻿using System.Linq;

public static class StringExtensions
{
    public static bool IsEmptyOrWhiteSpace(this string value)
    {
        return value.All(char.IsWhiteSpace);
    }
}
